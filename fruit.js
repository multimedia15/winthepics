var wrong_sound = new Audio("FFruit/Sound/wrong.mp3")
wrong_sound.oncanplaythrough = function () { }
wrong_sound.onended = function () { }

var correct_sound = new Audio("FFruit/Sound/correct.mp3")
correct_sound.oncanplaythrough = function () { }
correct_sound.onended = function () { }

//Wrong1, 2
function showWrong() {
    let pic = document.getElementById("wrongShow");
    pic.src = "FFruit/Choices/check/wrong.png";
    pic.style.display = 'block';
    window.setTimeout("document.getElementById('wrongShow').style.display='none';", 300);
}
function showWrong2() {
    let pic = document.getElementById("wrongShow2");
    pic.src = "FFruit/Choices/check/wrong.png";
    pic.style.display = 'block';
    window.setTimeout("document.getElementById('wrongShow2').style.display='none';", 300);
}